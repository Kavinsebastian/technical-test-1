# Technical test ke 2

### Deskripsi

function ini adalah function untuk mengecek string/kata yang tidak mungkin di cek oleh kunci kombinasi.
dimana kunci ini memiliki 3 tombol dan masing-masing tombol memiliki 8 huruf.
di dalam function tersebut saya menggunakan built in function dari indexOf()
fungsinya untuk mengecek sebuah string atau number bahkan object pada array, disini saya memanfaatkan untuk mengecek setiap huruf nya.
huruf-huruf tersebut saya ambil dari masing-masing kata yang telah di gunakan.
maka dari task atau soal yang pertama, jawaban saya adalah C.hat.


untuk mengecek kata tersebut 

```javascript
console.log()
```
* anda harus memanggil console terlebih dahulu untuk melihat hasilnya

```javascript
console.log(<functionName>)
```
* panggil function nya

```javascript
console.log(combinationLock(check))
```

* dia dalam parameter masukan kata yang mau di check (dalam pembuatan function saya memberi nama pada parameter "check")

* jika sudah ctrl + s atau save kemudian buka terminal lalu ketik 

$ node < namafile >

berhubung saya namai file dengan technical-test1

maka 

$ node technical-test1

maka anda dapat melihat hasil nya jika kata tersebut dapat di coba di kunci kombinasi maka return nya adalah possible jika tidak maka return nya not possible.

terima kasih.