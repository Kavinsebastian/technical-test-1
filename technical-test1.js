// Combination-lock

// diketahui combination-lock memiliki 3 tombol/scrolling
// masing-masing tombol memiliki 8 huruf
// diketahui combination-lock telah mencoba beberapa kata berikut
// one, who, two, bob, add, owl, fab, den, mia, dan tat.
// dan kata yang belum di coba adalah:
// win, deb, hat, men, dan bed

// pertanyaan nya mana kata yang tidak mungkin untuk di coba ?
// buatlah function untuk mengetahui bahwa kata tersebut tidak mungkin untuk di coba.


const combinationLock = (check) => {
    const tombol1 = ['o','w','t','b','a','f','d','m']
    const tombol2 = ['n','h','w','o','d','a','e','i']
    const tombol3 = ['e','o','b','d','l','n','a','t']
    if(tombol1.indexOf(check[0]) !== -1 && tombol2.indexOf(check[1]) !== -1 && tombol3.indexOf(check[2]) !== -1){
        return 'possible'
    }
        return 'not possible'
    
}

console.log(combinationLock('win')); //return possible
console.log(combinationLock('hat')); //return not possible
console.log(combinationLock('men')); //return possible
console.log(combinationLock('bad')); //return possible